import { Component, OnInit } from '@angular/core';
import { ReCaptchaV3Service } from 'ngx-captcha';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  // This constant is required for ReCaptcha, which is used by KOR Connect
  siteKey: string = 'your snippet site key';

  constructor(
    private reCaptchaV3Service: ReCaptchaV3Service,
    private http: HttpClient
  ) {}

  ngOnInit(): void {}

  // This function is an example of how to call your API through KOR Connect
  requestApi() {
    this.reCaptchaV3Service.execute(this.siteKey, 'homepage', (token) => {
      /* We'll need this constant to make request */
      const timestamp = new Date().toUTCString();
      const headers = {
        // Place your headers here:
        token,
        timestamp,
        'x-api-key': 'Your snippet x-api-key',
      };
      // You need to append the path of the endpoint you are calling to the KOR Connect base URI
      this.http
        .get('Your snippet endpoint', { headers })
        .subscribe((response) => {
          console.log(response);
        });
    });
  }
}
